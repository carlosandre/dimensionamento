function trapezoidal(app)
	syms Q H
	J = app.trap_J.Value / 100;     % m/m
	M = 1 / tand(app.trap_M.Value); % m/m
	Qd = app.vazao_m3s.Value;       % m�/s
	n = app.trap_Materiais.Value;
	
	if app.trap_base_vezes.SelectedObject == app.milimetrosButton_2
		B = app.trap_B.Value / 1000;
	elseif app.trap_base_vezes.SelectedObject == app.polegadasButton_2
		B = app.trap_B.Value / 39.37007874;
	elseif app.trap_base_vezes.SelectedObject == app.alturaButton_2
		B = app.trap_B.Value * H;
	end
	
	Am = H * (B + M * H);
	Pm = B + 2 * H * sqrt(M+1);
	Rh = Am / Pm;

	V = Rh^(2/3) * sqrt(J) / n;
	Q = Am * V;
	H = double(vpasolve(Q == Qd, H, 0.5));
	
	app.RespostaFinal.Text = ['Resposta:  B = ' num2str(double(subs(B))) ', H = ' num2str(H) ];
end