function circular(app)
	syms Q H D
	J = app.circ_J.Value / 100; % m/m
	Qd = app.vazao_m3s.Value;   % m�/s
	n = app.circ_Materiais.Value;
	ang = 0;
	
	if app.circ_altura_vezes.SelectedObject == app.grausButton
		ang = app.circ_H.Value * pi / 180;
	elseif app.circ_altura_vezes.SelectedObject == app.radianosButton
		ang = app.circ_H.Value;
	elseif app.circ_altura_vezes.SelectedObject == app.dimetroButton
		ang = 2 * acos( 1 - 2 * app.circ_H.Value );
	end
	
	Am = (ang - sin(ang)) * D^2 / 8;
	Pm = ang * D / 2;
	Rh = Am / Pm;

	V = Rh^(2/3) * sqrt(J) / n;
	Q = Am * V;
	D = double(vpasolve(Q == Qd, D, 0.5));
	
	app.RespostaFinal.Text = ['Resposta:  D = ' num2str(double(subs(D)) * 1000) ' mm ( ' num2str(double(subs(D)) * 39.37007874) ' in )' ];
	
	amrh = double(subs(Am) * subs(Rh)^(2/3));
	plotx = linspace(0, 2, 100);
	plotQ = linspace(Qd, Qd, 100);
	plotJx = [ J*100 J*100 ];
	plotJy = [ 0 Qd ];
	ploty1 = amrh .* sqrt(plotx./100) ./ 0.011;
	ploty2 = amrh .* sqrt(plotx./100) ./ 0.012;
	ploty3 = amrh .* sqrt(plotx./100) ./ 0.013;
	ploty4 = amrh .* sqrt(plotx./100) ./ 0.015;
	
	plot(app.grafico_decl, plotx, plotQ, ':', plotJx, plotJy, ':', ...
		plotx, ploty1, plotx, ploty2, ...
		plotx, ploty3, plotx, ploty4);
end