function kabc(app, cidade)
% K-A-B-C: retorna os dados de chuvas cr�ticas de uma determinada cidade
%          a cidade analisada � retornada atrav�s do valor `cidade`
%          (num�rico) selecionado na lista do programa.

if cidade == 0
	app.Cidades.BackgroundColor = 'yellow';
	return
else
	app.Cidades.BackgroundColor = 'white';
end

dados_cidades = [
    %  k    a    b    c
    702.71 0.24 8.85 0.74  % 1: Caxias do Sul / RS
    627.54 0.31 7.90 0.74  % 2: Porto Alegre / RS
    2000 0.108 20.2 0.838  % 3: S�o Paulo / SP
    777.44 0.13 3.50 0.67  % 4: Alegrete / RS
    604.90 0.21 3.25 0.72  % 5: Bag� / RS
    863.25 0.14 3.60 0.70  % 6: Cruz Alta / RS
    431.09 0.19 3.70 0.64  % 7: Encruzilhada do Sul / RS
    598.65 0.20 4.40 0.67  % 8: Ira� / RS
    670.74 0.21 7.90 0.74  % 9: Passo Fundo / RS
    774.14 0.23 6.90 0.74  % 10: Rio Grande / RS
    870.38 0.24 15.2 0.73  % 11: Santa Maria / RS
    1036.5 0.28 22.8 0.77  % 12: Santa Vit�ria do Palmar / RS
    1038.51 0.15 6.0 0.76  % 13: S�o Luiz Gonzaga / RS
];

	kabc = dados_cidades(cidade,:);
	app.K.Value = kabc(1);
	app.A.Value = kabc(2);
	app.B.Value = kabc(3);
	app.C.Value = kabc(4);
end

