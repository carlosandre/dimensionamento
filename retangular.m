function retangular(app)
	syms Q H
	J = app.ret_J.Value / 100;     % m/m
	Qd = app.vazao_m3s.Value;       % m�/s
	n = app.ret_Materiais.Value;
	
	if app.ret_base_vezes.SelectedObject == app.milimetrosButton
		B = app.ret_B.Value / 1000;
	elseif app.ret_base_vezes.SelectedObject == app.polegadasButton
		B = app.ret_B.Value / 39.37007874;
	elseif app.ret_base_vezes.SelectedObject == app.alturaButton
		B = app.ret_B.Value * H;
	end
	
	Am = H * B;
	Pm = B + 2 * H;
	Rh = Am / Pm;

	V = Rh^(2/3) * sqrt(J) / n;
	Q = Am * V;
	H = double(vpasolve(Q == Qd, H, 0.5));
	
	app.RespostaFinal.Text = ['Resposta:  B = ' num2str(double(subs(B))) ...
		' m; H = ' num2str(H) ' m; (B/H) = ' num2str(double(subs(B)/subs(H)))];
	
	amrh = double(subs(Am) * subs(Rh)^(2/3));
	plotx = linspace(0, 2, 100);
	plotQ = linspace(Qd, Qd, 100);
	plotJx = [ J*100 J*100 ];
	plotJy = [ 0 Qd ];
	ploty1 = amrh .* sqrt(plotx./100) ./ 0.011;
	ploty2 = amrh .* sqrt(plotx./100) ./ 0.012;
	ploty3 = amrh .* sqrt(plotx./100) ./ 0.013;
	ploty4 = amrh .* sqrt(plotx./100) ./ 0.015;
	
	plot(app.grafico_decl, plotx, plotQ, ':', plotJx, plotJy, ':', ...
		plotx, ploty1, plotx, ploty2, ...
		plotx, ploty3, plotx, ploty4);
end