function vazao(app)
	vazao = app.Intensidade.Value * app.Area.Value / 3600000;
	app.vazao_m3s.Value = vazao;
	app.vazao_Ls.Value = vazao * 1e3;
	app.vazao_Lmin.Value = vazao * 50/3;
end